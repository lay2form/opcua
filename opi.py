#!/usr/bin/python3

from opcua import Server
from random import randint
import time


class CustomServer:
    def __init__(self):
        self.server = None
        self.name = None
        self.addspace = None
        self.node = None
        self.machine_name = []
        self.machine = []

    # create the server object
    def create_server(self,url):
        self.server = Server()
        # define the server ip address and port number
        # url = "opc.tcp://0.0.0.0:2020"
        self.server.set_endpoint(url)
        print("Server object created at: {} url".format(url))

    def create_address_space(self,name):
        # create the address space for the server
        self.addspace = self.server.register_namespace(name)
        self.name = name
        print("The address space created under the name: {}".format(self.name))
        self.node_definition()

    def node_definition(self):
        # define the node object
        self.node = self.server.get_objects_node()
        print("Node object created...")

    def machine_iteration(self):
        # iterate over all the machines (nodes of the address space)
        for i in self.machine_name:
            self.machine.append(self.node.add_object(self.addspace, i))
        print("Iteration over. Machines created: {}".format(len(self.machine_name)))


if __name__ == "__main__":

    s = CustomServer()

    # define the server
    url = "opc.tcp://0.0.0.0:4040"
    s.create_server(url)

    # define the lab name
    name = "production_lab"

    # create the address space for the server
    s.create_address_space(name)

    # define the machines names
    # s.machine_name.append("rpi0")
    # s.machine_name.append("rpi1")
    s.machine_name.append("opi")

    # iterate ocer all the machines defined
    s.machine_iteration()

    # define the variables for each machine
    # # machine 0
    # Temperature_before = s.machine[0].add_variable(s.addspace, "Temperature_before", 100)
    # Temperature_after = s.machine[0].add_variable(s.addspace, "Temperature_after", 50)
    # Pressure = s.machine[0].add_variable(s.addspace, "Pressure", 2)
    # Temperature_before.set_writable()
    # Temperature_after.set_writable()
    # Pressure.set_writable()

    # # machine 1
    # Temperature_central = s.machine[0].add_variable(s.addspace, "Temperature_central", 120)
    # Temperature_right = s.machine[0].add_variable(s.addspace, "Temperature_right", 80)
    # Temperature_left = s.machine[0].add_variable(s.addspace, "Temperature_left", 85)
    # Force_front = s.machine[0].add_variable(s.addspace, "Force_front", 10)
    # Force_rear = s.machine[0].add_variable(s.addspace, "Force_rear", 12)
    # Temperature_central.set_writable()
    # Temperature_right.set_writable()
    # Temperature_left.set_writable()
    # Force_front.set_writable()
    # Force_rear.set_writable()
    #
    # machine 2
    Upper_plate_temperature = s.machine[0].add_variable(s.addspace, "Upper_plate_temperature", 25)
    Lower_plate_temperature = s.machine[0].add_variable(s.addspace, "Lower_plate_temperature", 26)
    Force_lc_0 = s.machine[0].add_variable(s.addspace, "Force_lc_0",1)
    Force_lc_1 = s.machine[0].add_variable(s.addspace, "Force_lc_1",1)
    Force_lc_2 = s.machine[0].add_variable(s.addspace, "Force_lc_2",1)
    Force_lc_3 = s.machine[0].add_variable(s.addspace, "Force_lc_3",1)
    Oil_pressure = s.machine[0].add_variable(s.addspace, "Oil_pressure", 2.4)
    Upper_plate_temperature.set_writable()
    Lower_plate_temperature.set_writable()
    Force_lc_0.set_writable()
    Force_lc_1.set_writable()
    Force_lc_2.set_writable()
    Force_lc_3.set_writable()
    Oil_pressure.set_writable()


    s.server.start()
    print("Server started")

    while True:
        Upper_plate_temperature.set_value(randint(200,250))
        Lower_plate_temperature.set_value(randint(200,250))
        Force_lc_0.set_value(randint(1,5))
        Force_lc_1.set_value(randint(1,5))
        Force_lc_2.set_value(randint(1,5))
        Force_lc_3.set_value(randint(1,5))
        Oil_pressure.set_value(randint(1,4))
        time.sleep(0.1)
