#!/usr/bin/python3

from opcua import Client
from random import randint
import time

class CustomClient:
    def __init__(self):
        self.servers_variables = []

if __name__ == "__main__":

    # define the client's address
    url1 = "opc.tcp://192.168.1.82:4840"
    client1 = Client(url1)
    client1.connect()

    # get the root node
    root = client1.get_root_node()

    # get the childrens
    child_root = root.get_children()

    # number 0 means the Objects folder
    child = child_root[0].get_children()
    # print(child)

    # numer 1 means the node created for this server
    node_name = child[1].get_browse_name()
    print(node_name)

    # get the variables involved in that node
    vars = child[1].get_variables()
    print("There are {} variables".format(len(vars)))

    for i in range(len(vars)):
        # show the variable name
        a = vars[i].get_browse_name().to_string().split(":")[-1]
        b = vars[i].get_value()
        print(a + ":",b)

    client1.disconnect()
