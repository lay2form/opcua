# README #

Create custom made OPC UA servers for different machines

### What is this repository for? ###

* To create an OPC UA server
* Create and run an OPC UA service on Ubuntu/Debian

### Install all the dependencies ###

On Ubuntu/Debian, do:

* sudo apt install python3-setuptools
* Python > 3.4: cryptography, dateutil, lxml and pytz (using apt install python3-...)

* sudo nano /lib/systemd/system/opc_server.service
* Write the opc_server.service content
* sudo chmod 644 /lib/systemd/system/opc_server.service
* sudo systemctl daemon-reload
* sudo systemctl enable opc_server.service
* sudo systemctl start opc_server.service

To check if the service is running:

* sudo systemctl status opc_server.service

In case you need to restart the service:

* sudo systemctl restart opc_server.service
